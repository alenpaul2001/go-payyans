package main

import (
	"fmt"
)

func main() {
	fmt.Println("some whit")
	fmt.Println(GetRules("normalizer/rules/normalizer_ml.rules"))
}

func ASCIIToUnicode(ASCIIText, font string) {
	fmt.Println(ASCIIText, font)
}

func GetRules(font string) (map[string]string, error) {
	return ReadAndCleanFile(font)
}
